function res = example_linear_reach_ARCH20_iss_ISSF01_ISU01()
% example_linear_reach_ARCH20_iss_ISSF01_ISU01 - ISS example from ARCH20
%   using the decomposed reachability algorithm following [1]
%
%
% Syntax:
%    example_linear_reach_ARCH20_iss_ISSF01_ISU01
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: -
%
% References: 
%   [1] S. Bogomolov, M. Forets, G. Frehse, A. Podelski, C. Schlling
%       "Decomposing Reach Set Computations with Low-dimensional Sets
%            and High-Dimensional Matrices"
% 
% Author:       Mark Wetzlinger
% Written:      07-June-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------


% load system matrices
load iss.mat A B C

% construct linear system
sys = linearSys('iss',A,B,[],C);
dim = length(A);

% Options -----------------------------------------------------------------
R0 = interval(-0.0001*ones(dim,1),0.0001*ones(dim,1));
params.R0             = zonotope(R0);
options.taylorTerms   = 6;
options.zonotopeOrder = 30;
options.linAlg        = 'decomp';
% options.saveOrder     = 3;
options.verbose       = true; % for logging every n iterations

options.specification = {halfspace([0, 0, 1], -5e-4); halfspace([0, 0, -1], -5e-4)};
options.partition     = [1, 135; 136, dim];

U             = interval([0;0.8;0.9],[0.1;1;1]);
params.uTrans = center(U); % center of input set
params.U      = zonotope([zeros(3,1),diag(rad(U))]); % input for reachability analysis
options.originContained = 0;

params.tFinal   = 20; % final time
params.timeStep = 0.01;
% -------------------------------------------------------------------------


% reachability analysis ---------------------------------------------------
tic
[Rout, ~, res] = reach(sys, params, options);
tComp = toc;
disp(['computation time of reachable set (decomp, sparse): ',num2str(tComp)]);
if ~res
    disp('Safety violation in reachability analysis!');
end
% -------------------------------------------------------------------------



% plot results ------------------------------------------------------------
hold on
i = 1;
options.blocks = length(options.partition);
relsafetyDim = 3;
% plot time elapse
while i <= length(Rout)
    % get Uout 
    t1 = (i-1)*options.timeStep;
    try
        minVal = inf;
        maxVal = -inf;
        for b=1:options.blocks
            if isa(Rout{i}{b},'zonotope')
                Uout = interval(project(Rout{i}{b},relsafetyDim));
                if infimum(Uout) < minVal
                    minVal = infimum(Uout);
                end
                if supremum(Uout) > maxVal
                    maxVal = supremum(Uout);
                end
            end
        end
        i = i + 1;
    catch
        for b=1:options.blocks
            if isa(Rout{i-1}{b},'zonotope')
                Uout = interval(project(Rout{i-1}{b},relsafetyDim));
                if infimum(Uout) < minVal
                    minVal = infimum(Uout);
                end
                if supremum(Uout) > maxVal
                    maxVal = supremum(Uout);
                end
            end
        end
    end
    t2 = (i-1)*options.timeStep;
    % generate plot areas as interval hulls
    IH1 = interval([t1; minVal], [t2; maxVal]);

    plotFilled(IH1,[1 2],[.75 .75 .75],'EdgeColor','none');
end

% plot unsafe set
plot(options.specification{1},[2,3]);
plot(options.specification{2},[2,3]);

% formatting
box on
xlabel('Time');
ylabel('y_3');
title('ISSF01-ISU01, reachDecomp');
axis([0, 20, -6e-4, 6e-4]);
grid on
%--------------------------------------------------------------------------

%example completed
res = 1;

%------------- END OF CODE --------------

