function completed = example_hybrid_reach_ARCH20_powerTrain_DTN02()
% example_hybrid_reach_ARCH20_powerTrain_DTN02 - powertrain example 
%                                                (see [1]) from the 2020
%                                                ARCH competition
%                                                (Test case DTN02)
% 
%
% Syntax:  
%    example_hybrid_reach_ARCH20_powerTrain_DTN02()
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% References:
%   [1] M. Althoff and B. H. Krogh. "Avoiding geometric intersection 
%       operations in reachability analysis of hybrid systems. HSCC 2012.
% 
% Author:       Matthias Althof, Niklas Kochdumper
% Written:      21-September-2011
% Last update:  12-March-2019
% Last revision:---


%------------- BEGIN CODE --------------


% System Dynamics ---------------------------------------------------------

[HA, Zcenter, Zdelta, B1, B2, B3, c1, c2, c3] = initPowerTrain(11); 
Zdelta = 0.3*Zdelta;


% Parameter  --------------------------------------------------------------

% initial set, initial location, and final time
params.R0 = zonotope([Zcenter,Zdelta]);
params.startLoc = 3;
params.tFinal = 0.2; 

% system input
u = [-5; 0];            % acceleration and load torque

params.U{1} = B1*zonotope(0) + B1*u + c1;
params.U{2} = B2*zonotope(0) + B2*u + c2; 
params.U{3} = B3*zonotope(0) + B3*u + c3;



% Reachability Settings ---------------------------------------------------

% continuous reachability
options.taylorTerms = 20;
options.zonotopeOrder = 20;

options.timeStepLoc{1} = 5e-4;
options.timeStepLoc{2} = 5e-4;
options.timeStepLoc{3} = 5e-4;

% intersection with guard sets
options.guardIntersect = 'zonoGirard';
options.enclose = {'pca'};



% Simulation 1 ------------------------------------------------------------

% simulation parameter
paramsSim = params;
paramsSim = rmfield(paramsSim,'U');
paramsSim = rmfield(paramsSim,'R0');

paramsSim.u{1} = center(params.U{1});
paramsSim.u{2} = center(params.U{2});
paramsSim.u{3} = center(params.U{3});

% obtain random simulation results
simRes1 = cell(10,1);

for i = 1:length(simRes1)
    
    % set initial state, input
    if i == 1
        paramsSim.x0 = Zcenter + Zdelta;
    elseif i == 2
        paramsSim.x0 = Zcenter - Zdelta;
    else
        paramsSim.x0 = randPoint(params.R0);
    end 
    
    %simulate hybrid automaton
    HAsim = simulate(HA,paramsSim); 
    simRes1{i} = get(HAsim,'trajectory');
end




% Reachability Analysis 1 -------------------------------------------------

tStart = tic;
HA_1 = reach(HA,params,options);
tComp1 = toc(tStart);



% Parameter ---------------------------------------------------------------

% update initial set
Rtemp = get(HA_1,'reachableSet');
params.R0 = Rtemp{end}.T{end};

% update start and final time
params.tStart = 0.2;
params.tFinal = 2;

% update system input
u = [5; 0];             % acceleration and load torque

params.U{1} = B1*zonotope(0) + B1*u + c1;
params.U{2} = B2*zonotope(0) + B2*u + c2; 
params.U{3} = B3*zonotope(0) + B3*u + c3;



% Simulation 2 ------------------------------------------------------------

% simulation parameter
paramsSim = params;
paramsSim = rmfield(paramsSim,'U');
paramsSim = rmfield(paramsSim,'R0');

paramsSim.u{1} = center(params.U{1});
paramsSim.u{2} = center(params.U{2});
paramsSim.u{3} = center(params.U{3});

% obtain random simulation results
simRes2 = cell(length(simRes1),1);

for i = 1:length(simRes1)
    
    % set initial state
    paramsSim.x0 = simRes1{i}.x{1}(end,:); 
    
    % simulate hybrid automaton
    HAsim = simulate(HA,paramsSim); 
    simRes2{i} = get(HAsim,'trajectory');
end



% Reachability Analysis 2 -------------------------------------------------

tStart = tic;
HA_2 = reach(HA,params,options);
tComp2 = toc(tStart);

disp(['computation time: ',num2str(tComp1 + tComp2)]);



% Visualization -----------------------------------------------------------

figure; hold on

% plot reachable set
plotReachFilled(HA_1,[1,3],[.6 .6 .6]);
plotReachFilled(HA_2,[1,3],[.6 .6 .6]);

% plot simulation
for i = 1:length(simRes1)
    for j = 1:length(simRes1{i}.x)
        plot(simRes1{i}.x{j}(:,1),simRes1{i}.x{j}(:,3),'k');
    end
end
for i = 1:length(simRes2)
    for j = 1:length(simRes2{i}.x)
        plot(simRes2{i}.x{j}(:,1),simRes2{i}.x{j}(:,3),'k');
    end
end

% guard set
plot([0.03 0.03],[-10 90],'r');
plot([-0.03 -0.03],[-10 90],'r');

% formatting
xlabel('x_1');
ylabel('x_3');
xlim([-0.1,0.2]);
ylim([-10,90]);
box on


% example completed
completed = 1;

%------------- END OF CODE --------------
