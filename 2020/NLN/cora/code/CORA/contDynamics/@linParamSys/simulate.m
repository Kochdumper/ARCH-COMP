function [t,x,ind] = simulate(obj,params,options)
% simulate - simulates the linear interval system within a location
%
% Syntax:  
%    [t,x,ind] = simulate(obj,params,options)
%
% Inputs:
%    obj - linParamSys object
%    params - struct containing the parameters for the simulation
%       .tStart initial time t0
%       .tFinal final time tf
%       .x0 initial point x0
%       .u piecewise constant input signal u(t) specified as a matrix
%           for which the number of rows is identical to the number of
%           system input
%       .p parameter value p (class nonlinParamSys only)
%       .startLoc index of the initial location (class hybridAutomaton and
%           parallelHybridAutomaton only)
%       .finalLoc index of the final location (class hybridAutomaton and
%           parallelHybridAutomaton only)
%    options - ODE45 options (for hybrid systems)
%
% Outputs:
%    t - time vector
%    x - state vector
%    ind - returns the event which has been detected
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      16-May-2007 
% Last update:  07-January-2009
%               08-May-2020 (MW, update interface)
% Last revision: ---

%------------- BEGIN CODE --------------

%sample system matrix
tmp = randomSampling(obj.A,1);
obj.sampleMatrix.A = tmp{1};

if isempty(options.Events)
    [t,x] = ode45(getfcn(obj,params),...
        [params.tStart,params.tFinal],params.x0,options);
else
    [t,x,te,xe,ind] = ode45(getfcn(obj,params),...
        [params.tStart,params.tFinal],params.x0,options);
end
    
    
%------------- END OF CODE --------------