#include <ibex.h>
#include "vibes.cpp"

using namespace ibex;

//#define __V__ 


unsigned long int i = 0;
void toGunPlot (const simulation* sim) {

  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
    i++;
    std::cout << "set object " << i <<  " rect from " <<
      iterator_list->time_j.lb() << ", " <<
      iterator_list->box_j1->operator[](3).lb() << " to " <<
      iterator_list->time_j.ub() << ", " <<
      iterator_list->box_j1->operator[](3).ub() <<
       std::endl;
  }

}

void plot_simu(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
    vibes::drawBox(iterator_list->time_j.lb(),
    iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](3).lb(), iterator_list->box_j1->operator[](3).ub(), "blue[blue]");  

  }
}



int main(){

  const int n = 7;
  Variable y(n);

  ibex::Array<const ibex::ExprNode> a(n);
  a.set_ref(0, 1.4 * y[2] - 0.9 * y[0]);
  a.set_ref(1, 2.5 * y[4] - 1.5 * y[1]);
  a.set_ref(2, 0.6 * y[6] - 0.8 * y[1] * y[2]);
  a.set_ref(3, 2.0 - 1.3 * y[2]*y[3]);
  a.set_ref(4, 0.7 * y[0] - y[3]*y[4]);
  a.set_ref(5, 0.3 * y[0] - 3.1 * y[5]);
  a.set_ref(6, 1.8 * y[5] - 1.5 * y[1] * y[6]);

  const ibex::ExprVector& _return = ibex::ExprVector::new_(a, true);
  ibex::Function ydot(y, _return);

  //Interval eps(0.0,0.0);
  Interval eps(-0.01,0.01); // can be solved with KUTTA3 with prec = 1e-5 and noise number = 50
  IntervalVector yinit(n);
  yinit[0] = Interval(1.2)+eps;
  yinit[1] = Interval(1.05)+eps;
  yinit[2] = Interval(1.5)+eps;
  yinit[3] = Interval(2.4)+eps;
  yinit[4] = Interval(1.0)+eps;
  yinit[5] = Interval(0.1)+eps;
  yinit[6] = Interval(0.45)+eps;

  double prec = 1e-6;
  double end = 20;

  //  AF_fAFFullI::setAffineTolerance (5e-16);
  AF_fAFFullI::setAffineNoiseNumber (50);

  ivp_ode problem = ivp_ode(ydot, 0.0, yinit);
  simulation simu = simulation(&problem, end, KUTTA3, prec); // starting value 0.01 ?
  simu.setHmin(5e-4);
  //simu.active_monotony();
  simu.run_simulation();

  IntervalVector safebox(n, Interval::ALL_REALS);
  safebox[2] = Interval(NEG_INFINITY, 4.49);
  if(simu.stayed_in(safebox)) {
    //std::cout << "safe success" << std::endl;
  }

#ifdef __V__

  std::cout << "set terminal png" << std::endl;
  std::cout << "set output 'laub-loomis-tight.png'" << std::endl;
  std::cout << "set xrange [0:20]" << std::endl;
  std::cout << "set yrange [0:5]" << std::endl;
  std::cout << "set nokey" << std::endl;
  std::cout << "set style rectangle back fc rgb 'orange' fs solid 1.0 noborder" << std::endl;

  toGunPlot (&simu);

  std::cout << "plot 4.5 lt rgb 'black', 5 lt rgb 'black'" << std::endl;

#endif

  vibes::beginDrawing ();
  vibes::selectFigure("Laub Loomis");

  
  plot_simu(&simu);
  
       std::cout << "Diam x4 : " << (simu.get_last())[3].diam() << std::endl;
      
 //vibes::axisLimits(0,20, 0, 5);
 vibes::axisAuto();
  //vibes::setFigureProperty("Laub Loomis Tight","time",20);
  //vibes::setFigureProperty("Laub Loomis Tight","y",5);
  vibes::setFigureProperty("Laub Loomis","width",1920);
  vibes::setFigureProperty("Laub Loomis","height",1080);
  vibes::saveImage("Laub_loomis_3.jpg","Laub Loomis");
    
  vibes::closeFigure("Laub Loomis");  
  vibes::endDrawing();
  return 0;

}
