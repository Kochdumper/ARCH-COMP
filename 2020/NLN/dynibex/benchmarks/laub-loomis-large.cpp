#include <ibex.h>
#include <queue>
#include <iostream>
#include "vibes.cpp"


using namespace ibex;
///Van der Pol

#define __PREC__ 1e-7
#define __METH__ KUTTA3
#define __DURATION__ 20

void plot_simu(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {


    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](3).lb(),
		   iterator_list->box_j1->operator[](3).ub(),
		   "red[red]");

  }
}

int main(){


  const int n = 7;
  Variable y(n);

  ibex::Array<const ibex::ExprNode> a(n);
  a.set_ref(0, 1.4 * y[2] - 0.9 * y[0]);
  a.set_ref(1, 2.5 * y[4] - 1.5 * y[1]);
  a.set_ref(2, 0.6 * y[6] - 0.8 * y[1] * y[2]);
  a.set_ref(3, 2.0 - 1.3 * y[2]*y[3]);
  a.set_ref(4, 0.7 * y[0] - y[3]*y[4]);
  a.set_ref(5, 0.3 * y[0] - 3.1 * y[5]);
  a.set_ref(6, 1.8 * y[5] - 1.5 * y[1] * y[6]);

  const ibex::ExprVector& _return = ibex::ExprVector::new_(a, true);
  ibex::Function ydot(y, _return);

  Interval err(-0.1,0.1);
  IntervalVector yinit(n);
  yinit[0] = Interval(1.2)+err;
  yinit[1] = Interval(1.05)+err;
  //yinit[1] = Interval(1.105)+err;
  yinit[2] = Interval(1.5)+err;
  yinit[3] = Interval(2.4)+err;
  yinit[4] = Interval(1.0)+err;
  yinit[5] = Interval(0.1)+err;
  yinit[6] = Interval(0.45)+err;


  queue<IntervalVector> s;
  s.push (yinit);

  //  AF_fAFFullI::setAffineTolerance (1e-16);
  AF_fAFFullI::setAffineNoiseNumber (80);

  // Precision (boxes of size less than eps are not processed)
  double eps=1e-2;

  for (int i = 0; i < 64; i++) { // 15 is a good number of bissections
    IntervalVector box = s.front();
    s.pop();
    if (box.max_diam() >= eps) {
      LargestFirst bbb(eps, 0.5);
      pair<IntervalVector,IntervalVector> p=bbb.bisect(box);
      s.push(p.first);
      s.push(p.second);
    }
    else {
      std::cout << "Trop petit " << box << std::endl;
      s.push (box);
    }
  }

  vibes::beginDrawing ();
  vibes::newFigure("Laub Loomis");

  int len = s.size();
  int cpt = 1;
  
  IntervalVector last(n);
  bool first=true;
  
  
  while (!s.empty()) {
    IntervalVector box = s.front();
    s.pop();
    //std::cout << "cpt = " << cpt << " / " << len <<
     // " -- current box = " << box << std::endl;
    ivp_ode problem = ivp_ode (ydot, 0.0, box);
    simulation simu = simulation (&problem, __DURATION__, __METH__, __PREC__, 0.01);
    //simu.setHmin(1e-4);
    //simu.get_attractor();
    simu.run_simulation();

    if (first)
    {
    	last=simu.get_last();
    	first = false;
    }
    else
    	last|=simu.get_last();
   

    plot_simu(&simu);
    cpt++;
  }


  // Safety zones
  // vibes::drawBox( 0.0,
  // 		  20.0,
  // 		  1.0,
  // 		  4.5,
  // 		  "black");

  // vibes::drawBox( 0.0,
  // 		  20.0,
  // 		  1.0,
  // 		  4.3,
  // 		  "red");


  std::cout << "Diam x4 : " << last[3].diam() << std::endl;
  
  
 vibes::axisAuto();
  vibes::setFigureProperty("Laub Loomis","width",1920);
  vibes::setFigureProperty("Laub Loomis","height",1080);
    vibes::saveImage("Laub_loomis_1.jpg","Laub Loomis");
  vibes::endDrawing();
  return 0;
}
