import numpy as np
np.random.seed(7)

from keras.models import Sequential
from keras.layers import Dense
import pylab as pl

X = np.linspace(-0.5 * np.pi, 0.5 * np.pi, 10000).reshape(-1, 1)
Y = np.sin(X)#_degrees)

model = Sequential()
model.add(Dense(16, input_dim=X.shape[1], kernel_initializer='uniform', activation='relu'))
model.add(Dense(8, kernel_initializer='uniform', activation='relu'))
#model.add(Dense(40, kernel_initializer='uniform', activation='relu'))
model.add(Dense(1, kernel_initializer='uniform', activation='linear'))

model.compile(loss='mse', optimizer='adam', metrics=['mae']) 
model.fit(X, Y, epochs=100, batch_size=32, verbose=2)

res = model.predict(X, batch_size=32)

pl.subplot(211)
pl.plot(res, label='ann')
pl.plot(Y, label='train')
pl.xlabel('#')
pl.ylabel('value [arb.]')
pl.legend()
pl.subplot(212)
pl.plot(Y - res, label='diff')
pl.legend()
pl.savefig("x_sin2.png")
#pl.show()

model.save("x_sin2.h5")
