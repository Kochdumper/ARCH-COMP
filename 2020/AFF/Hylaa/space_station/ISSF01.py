'''
This is the International Space Station example used in the ARCH reachability tools competition in 2019.

It is a 271-dimensional continuous system, with three time-varying inputs.
'''

import time
import sys

from matplotlib import collections

from scipy.io import loadmat

from hylaa.hybrid_automaton import HybridAutomaton
from hylaa.settings import HylaaSettings, PlotSettings
from hylaa.core import Core
from hylaa.stateset import StateSet
from hylaa import lputil, lpinstance

def make_automaton(limit=0.0005, spec=1):
    'make the hybrid automaton'

    ha = HybridAutomaton()

    mode = ha.new_mode('mode')
    dynamics = loadmat('iss.mat')
    a_matrix = dynamics['A']
    b_matrix = dynamics['B']

    mode.set_dynamics(a_matrix)

    # input bounds
    # 0 <= u1 <= 0.1
    # 0.8 <= u2 <= 1.0
    # 0.9 <= u3 <= 1.0
    bounds_mat = [[1, 0, 0], [-1, 0, 0], [0, 1, 0], [0, -1, 0], [0, 0, 1], [0, 0, -1]]
    bounds_rhs = [0.1, 0, 1.0, -0.8, 1.0, -0.9]
    mode.set_inputs(b_matrix, bounds_mat, bounds_rhs)

    error = ha.new_mode('error')

    # the third output defines the unsafe condition
    y3 = dynamics['C'][2]

    # Error condition: y3 * x <= -limit OR y3 >= limit
    if spec == 1:
        trans = ha.new_transition(mode, error)
        trans.set_guard(y3, [-limit])
        
    if spec == 2:
        trans = ha.new_transition(mode, error)
        trans.set_guard(-1 * y3, [-limit])

    return ha

def make_init(ha):
    'make the initial states'

    # initial set has every variable as [-0.0001, 0.0001]
    mode = ha.modes['mode']

    dims = mode.a_csr.shape[0]
    init_box = dims * [[-0.0001, 0.0001]]
    init_lpi = lputil.from_box(init_box, mode)
    
    init_list = [StateSet(init_lpi, mode)]

    return init_list

def make_settings(lines=None, filename=None):
    'make the reachability settings object'

    # see hylaa.settings for a list of reachability settings
    settings = HylaaSettings(0.01 if filename is None else 0.02, 20.0) # plot with courser settings to speed things up
    settings.plot.plot_mode = PlotSettings.PLOT_NONE
    settings.stdout = HylaaSettings.STDOUT_VERBOSE

    settings.plot.xdim_dir = None # x dimension will be time

    dynamics = loadmat('iss.mat')
    y3 = dynamics['C'][2]
    settings.plot.ydim_dir = y3.toarray()[0] # use y3 for the y plot direction

    if filename is not None:
        settings.plot.filename = filename
        settings.plot.plot_mode = PlotSettings.PLOT_IMAGE
        settings.plot.label.title = ''
        settings.plot.label.y_label = '$y_{3}$'
        settings.plot.label.x_label = '$t$'
        settings.plot.plot_size = (6, 6)
        settings.plot.label.axes_limits = (0, 20, -0.0008, 0.0008)

        # add lines
        line_pts = []
        
        for line in lines:
            tmax = settings.num_steps * settings.step_size
            
            line_pts.append([(0, line), (tmax, line)])

        c1 = collections.LineCollection(line_pts, animated=True, colors=('red'), linewidths=(2), linestyle='dashed')

        settings.plot.extra_collections = [[c1]]

    return settings

def run_hylaa():
    'main entry point'

    safe_limit = 0.0007
    unsafe_limit = 0.0005

    runtime = None

    if len(sys.argv) > 1 and sys.argv[1] == 'safe':
        # it's more efficient to check one spec at a time due to warm-start LP
        for spec in [1, 2]:
            print(f"Checking safe spec {spec}")
            ha = make_automaton(limit=safe_limit, spec=spec)
            init_states = make_init(ha)
            settings = make_settings()

            res = Core(ha, settings).run(init_states)
            assert not res.has_concrete_error

    elif len(sys.argv) > 1 and sys.argv[1] == 'unsafe':
        # check unsafe case
        for spec in [1, 2]:
            print(f"Checking unsafe spec {spec}")
            ha = make_automaton(limit=unsafe_limit, spec=spec)
            init_states = make_init(ha)
            settings = make_settings()

            res = Core(ha, settings).run(init_states)

            if res.has_concrete_error:
                break

        assert res.has_concrete_error, "expected error but didn't find it"

    elif len(sys.argv) > 1 and sys.argv[1] == 'plot':
        ha = make_automaton(limit=safe_limit, spec=None)
        init_states = make_init(ha)
        lines = [safe_limit, unsafe_limit, -unsafe_limit, -safe_limit]
        settings = make_settings(lines, filename='ISSF01.png')

        res = Core(ha, settings).run(init_states)
    else:
        print("expected single argument: safe/unsafe/plot")

    return not res.has_concrete_error

def save_csv():
    'run and save to csv'

    start = time.perf_counter()
    is_safe = run_hylaa()
    secs = time.perf_counter() - start

    if sys.argv[1] != 'plot':
        verified = 1 if is_safe else -1
        instance = "ISS01" if sys.argv[1] == 'safe' else 'ISSU02'

        with open("../results.csv", "a") as f:
            f.write(f"Hylaa,ISSF01,{instance},{verified},{secs},\n")

if __name__ == "__main__":
    save_csv()
