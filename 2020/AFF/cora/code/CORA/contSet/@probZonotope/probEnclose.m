function probZ = probEnclose(probZ,Ar,Rtrans)
% probEnclose - Encloses a probabilistic zonotope pZ and its map A*pZ
%    up to the m-sigma bound
%
% Syntax:  
%    probZ = probEnclose(probZ,A,Rtrans)
%
% Inputs:
%    probZ - probabilistic zonotope object
%    Ar - interval matrix of the linear map multiplied by the time increment
%    Rtrans - reachable set due to input deviation from the origin
%
% Outputs:
%    probZ - probabilistic zonotope object
%
% Example:
%    Z1 = [10 1 -2; 0 1 1];
%    Z2 = [0.6 1.2; 0.6 -1.2];
%    probZ = probZonotope(Z1,Z2);
%    A = rand(2,2);
%    Rtrans = zonotope([0;0],eye(2));
%    probEnclose(probZ,A,Rtrans)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      28-August-2007
% Last update:  29-February-2008
%               03-September-2009
%               08-September-2009
% Last revision: ---

%------------- BEGIN CODE --------------

%TO DO: change computation as it is in the dissertation!

%get dimension
d = dim(probZ);

%retrieve uncertain zonotope deltaZ
Zaux = (expm(Ar)-eye(d))*zonotope(probZ)+Rtrans;
deltaZ = enclose(zonotope(zeros(d,1)),Zaux);
probZ = probZ+deltaZ;

%change probabilistic generators if det(A)<1
if trace(Ar)<0
    if probZ.gauss
        probZ.cov=expm(Ar)*probZ.cov*expm(Ar)';
    else
        probZ.g=expm(Ar)*probZ.g;
    end
end

%------------- END OF CODE --------------