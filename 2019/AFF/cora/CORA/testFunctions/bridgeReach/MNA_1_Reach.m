function MNA_1_Reach()
% updated: 24-February-2017, MA

%start parallel computing
try 
    parpool
catch
    disp('parallel session already running')
end

% set Krylov
Krylov = 1;

% set output
output = 1;

%system matrix
load MNA_1.mat % built-in MATLAB example
C = B';

% % obtain A and B marices
% A = G.A;
% B = G.B;

%obtain dimension
dim = length(A);

%set options --------------------------------------------------------------
options.tStart = 0; %start time
options.tFinal = 1e-3; %final time
options.x0 = 100*ones(dim,1); %initial state for simulation
I = eye(dim);
options.R0=zonotope(sparse([options.x0,100*I(:,1:10)])); %initial state for reachability analysis

options.timeStep=1e-5; %time step size for reachable set computation
options.taylorTerms=4; %number of taylor terms for reachable sets
options.zonotopeOrder=10; %zonotope order
options.errorOrder=1;
options.polytopeOrder=2; %polytope order
options.reductionTechnique='girard'; %polytope order

options.plotType='frame';

%options.W = pinv(diag([0.1,0.1,0.5,10,1,1]));
options.originContained = 0;
options.reductionInterval = inf;
options.advancedLinErrorComp = 1;
options.tensorOrder = 2;

if Krylov
    options.KrylovError = eps;
    options.KrylovStep = 20;
else
    options.saveOrder = 1;
end
%--------------------------------------------------------------------------


%obtain uncertain inputs
options.uTrans = zeros(9,1);
options.U = zonotope([zeros(9,1), 0.1*eye(9)]);

%specify continuous dynamics-----------------------------------------------
if output
    sys=linearSys('MNA1',A,B,[],C); %initialize dynamics
else
    sys=linearSys('MNA1',A,B); %initialize dynamics
end
%--------------------------------------------------------------------------


%compute reachable set using zonotope bundles
tic
if output && ~Krylov
    [Rcont,Rcont_tp,Rcont_y] = reach(sys, options);
else
    Rcont = reach(sys, options);
end
toc 

%simulate
stepsizeOptions = odeset('MaxStep',0.2*(options.tFinal-options.tStart));
%generate overall options
opt = odeset(stepsizeOptions);

%initialize
runs=40;
finalTime=options.tFinal;

for i=1:runs

    %set initial state, input
    if i<=30
        options.x0=randPointExtreme(options.R0); %initial state for simulation
    else
        options.x0=randPoint(options.R0); %initial state for simulation
    end

    %set input
    if i<=8
        options.u=randPointExtreme(options.U)+options.uTrans; %input for simulation
    else
        options.u=randPoint(options.U)+options.uTrans; %input for simulation
    end

    %simulate hybrid automaton
    [sys,t{i},x{i}] = simulate(sys,options,options.tStart,options.tFinal,options.x0,opt);
    x_proj{i} = (sys.C*x{i}')';
end


%plot
for plotRun=1:4
    if plotRun==1
        projectedDimensions=[1 2];
    elseif plotRun==2
        projectedDimensions=[3 4];
    elseif plotRun==3
        projectedDimensions=[5 6];
    elseif plotRun==4
        projectedDimensions=[7 8]; 
    end

    figure;
    hold on

    %plot reachable sets of zonotope
    for i=1:length(Rcont)
        Zproj = project(Rcont{i},projectedDimensions);
        Zproj = reduce(Zproj,'girard',100);
        plotFilled(Zproj,[1 2],[.8 .8 .8],'EdgeColor','none');
    end

    %plot initial set
    plot(options.R0,projectedDimensions,'w-','lineWidth',2);

    %plot simulation results      
    for i=1:length(t)
            plot(x_proj{i}(:,projectedDimensions(1)),x_proj{i}(:,projectedDimensions(2)),'Color',0*[1 1 1]);
    end

    xlabel(['x_{',num2str(projectedDimensions(1)),'}']);
    ylabel(['x_{',num2str(projectedDimensions(2)),'}']);
end

% Visualization -----------------------------------------------------------
for plotRun=1:9
    figure
    hold on
    i = 1;
    iDim = plotRun;
    % plot time elapse
    while i<=length(Rcont)
        % get Uout 
        t1 = (i-1)*options.timeStep;
        try
            minVal = inf;
            maxVal = -inf;
            Uout = interval(project(Rcont{i},iDim));
            if infimum(Uout) < minVal
                minVal = infimum(Uout);
            end
            if supremum(Uout) > maxVal
                maxVal = supremum(Uout);
            end
            i = i + 1;
        catch
            minVal = infimum(interval(project(Rcont{i-1},iDim)));
            maxVal = supremum(interval(project(Rcont{i-1},iDim)));
        end
        t2 = (i-1)*options.timeStep;
        % generate plot areas as interval hulls
        IH1 = interval([t1; minVal], [t2; maxVal]);

        plotFilled(IH1,[1 2],[.75 .75 .75],'EdgeColor','none');
    end

    % plot simulation results
    for i=1:(length(t))
        plot(t{i},x_proj{i},'Color',0*[1 1 1]);
    end
    
    xlabel('t');
    ylabel(['y_{',num2str(iDim),'}']);
end