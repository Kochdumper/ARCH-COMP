function [E] = mtimes(A,E_in)
% mtimes - Overloaded '*' operator for the multiplication of a matrix with
% an ellipsoid
%
% Syntax:  
%    [E] = mtimes(A,E)
%
% Inputs:
%    A - numerical matrix
%    E - Ellipsoid object 
%
% Outputs:
%    E - Ellipsoid after multiplication of a matrix with an ellipsoid
%
% Example: 
%    E=ellipsoid([1 0; 0 1]);
%    matrix=[1 1; 1 1];
%    plot(E);
%    hold on
%    E=A*E;
%    plot(E);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plus

% Author:       Matthias Althoff
% Written:      30-September-2006 
% Last update:  07-September-2007
%               05-January-2009
%               06-August-2010
%               01-February-2011
%               08-February-2011
%               18-November-2015
% Last revision:---

%------------- BEGIN CODE --------------

%Find a ellipsoids object
%Is factor1 a ellipsoid?
if ~isa(E_in,'ellipsoid')
    error('Second factor has to be an ellipsoids object'); 
elseif size(A,2)~=length(E_in.Q)
     error('A must have same number of columns as Q has rows');
end

E=ellipsoid(A*E_in.Q*A',A*E_in.q);


%------------- END OF CODE --------------