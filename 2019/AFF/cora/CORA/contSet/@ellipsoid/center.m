function [c] = center(E)
% center - returns the center of an ellipsoid object
%
% Syntax:  
%    [c] = center(E);
%
% Inputs:
%    E - Ellipsoid object
%
% Outputs:
%    c - center of E
%
% Example: 
%    E1=ellipsoid([1 0; 0 1]);
%    c = center(E1);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      30-September-2006 
% Last update:  07-September-2007
%               05-January-2009
%               06-August-2010
%               01-February-2011
%               08-February-2011
%               18-November-2015
% Last revision:---

%------------- BEGIN CODE --------------
c = E.q;
%------------- END OF CODE --------------