function res = test_partition_center_functions()
% test of the functions centerSegment and segmentCenter

oneDimField=partition([0,10],5);
threeDimField=partition([0,10; -3,3; 0,1],[5;10;3]);
threeDimField_odd=partition([0,10; -3,3; 0,1],[5;11;3]);
threeDimField_div=partition({[0 2 3 4 8 10],[-3 -1.5 -1 -0.9 0 0.1 0.2 0.3 1 2 3],[0,0.3,0.6,1]});

c1 = centerSegment(oneDimField);
c2 = centerSegment(threeDimField);
c3 = centerSegment(threeDimField_odd);
c4 = centerSegment(threeDimField_div);

S1 = segmentCenter(oneDimField,c1);
S2 = segmentCenter(threeDimField,c2);
S3 = segmentCenter(threeDimField_odd,c3);
S4 = segmentCenter(threeDimField_div,c4);

res1 = (S1{1}-5)<1e-15;
res2 = norm(S2{1}-[5.0000  -0.3000  0.5000]')<1e-15;
res3 = norm(S2{2}-[5.0000  0.3000  0.5000]')<1e-15;
res4 = norm(S3{1}-[5.0000  0  0.5000]')<1e-15;
res5 = norm(S4{1}-[6.0000  -0.4500   0.4500]')<1e-15;
res6 = norm(S4{2}-[6.0000 0.0500 0.4500]')<1e-15;


res = res1&&res2&&res3&&res4&&res5&&res6;
