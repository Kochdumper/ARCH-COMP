function res = test_partition_get_intervals()
% test of the function segmentInterval

%setup partitions
threeDimField=partition([0,10; -3,3; 0,1],[5;10;3]);
twoDimField=partition([0,10; -3,3],[5;10]);
oneDimField=partition([0,10],5);

% check that segmentInterval works, 1DOF
Ints1 = segmentInterval(oneDimField,0:oneDimField.totalSegments);
Ints2 = segmentInterval(oneDimField);

if length(Ints1)==length(Ints2)
    res1 = (norm(supremum(Ints1{3}) - supremum(Ints2{3}))<1e-9)&&(norm(infimum(Ints1{3}) - infimum(Ints2{3}))<1e-9);
else
    res1 = 0;
end

% check that segmentInterval works, 2DOF
Ints1 = segmentInterval(twoDimField,0:twoDimField.totalSegments);
Ints2 = segmentInterval(twoDimField);

if length(Ints1)==length(Ints2)
    res2 = (norm(supremum(Ints1{3}) - supremum(Ints2{3}))<1e-9)&&(norm(infimum(Ints1{3}) - infimum(Ints2{3}))<1e-9);
else
    res2 = 0;
end


% check that segmentInterval works, 3DOF
Ints1 = segmentInterval(threeDimField,0:threeDimField.totalSegments);
Ints2 = segmentInterval(threeDimField);

if length(Ints1)==length(Ints2)
    res3 = (norm(supremum(Ints1{3}) - supremum(Ints2{3}))<1e-9)&&(norm(infimum(Ints1{3}) - infimum(Ints2{3}))<1e-9);
else
    res3 = 0;
end


res = res1&&res2&&res3;
% 
% segmentPolytope(threeDimField,[1 5 3])
% segmentPolytope(threeDimField)
% segmentZonotope(threeDimField,[1 5 3])
% segmentZonotope(threeDimField)
% P = mptPolytope([2 0 0.3;4 2 0.6;1 1 0.5; 1 1 0.1]);
% intersectingSegments(threeDimField,P)
% [iS,percentages] = exactIntersectingSegments(threeDimField,P)
% plot(threeDimField,exactIntersectingSegments(threeDimField,P))
% hold on
% plot(P)
% 
% %partition with 