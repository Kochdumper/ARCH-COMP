#!/bin/bash
mkdir KeYmaera_3.6.17

cd KeYmaera_3.6.17

wget -r -np -nd -nH -A.jar -e robots=OFF http://www.ls.cs.cmu.edu/KeYmaera/arch2018repeatability/KeYmaera_3.6.17

cd ..

echo "Please follow the remaining installation instructions in README.txt"